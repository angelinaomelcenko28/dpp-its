import random

def calcHist(tdata: list):
    hist = [0]*10
    for i in tdata:
        if 0<=i<100:
            hist[0] = hist[0] + 1
        elif 100<=i<200:
            hist[1] = hist[1] + 1
        elif 200<=i<300:
            hist[2] = hist[2] + 1
        elif 300<=i<400:
            hist[3] = hist[3] + 1
        elif 400<=i<500:
            hist[4] = hist[4] + 1
        elif 500<=i<600:
            hist[5] = hist[5] + 1
        elif 600<=i<700:
            hist[6] = hist[6] + 1
        elif 700<=i<800:
            hist[7] = hist[7] + 1
        elif 800<=i<900:
            hist[8] = hist[8] + 1
        elif 900<=i<1000:
            hist[9] = hist[9] + 1
    print (hist)

mas = []
for i in range(1000000):
    mas.append(random.randint(0,999))
calcHist(mas)
